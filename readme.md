# Kanban Board

- Kanban board has columns. Each column is named, and optionally user can set column's max capacity
- Cards are placed in columns and user can add and delete cards
- User can drag and drop them into a new position in the same column, or to another column
- When user sets column's capacity, that column can't have more the given number of cards in it
- Cards can't be moved to a column that reached it's capacity neither can cards be created in it

## Requirements

- [Node.js](https://nodejs.org/en/) - JavaScript run-time environment

## Installation

- Clone the repository
- Install dependencies (from console):
  - Navigate to React project
  - run `npm install`
- For more detailed information about react
  - [React](https://reactjs.org/) - JavaScript library for building user interfaces

## Running

```
npm start
```

Your application is now running on `http://localhost:3000`

## Enjoy!
