import axios from "axios";

const ENDPOINTS = {
  COLUMNS: "columns/",
  COLUMN_CARDS: "cards/status/",
  CARDS: "cards/",
  CARDS_ORDER: "cards/order/"
};
class MyService {
  constructor() {
    axios.defaults.baseURL = "http://127.0.0.1:8000/";
  }
  getBoardData() {
    return axios.get(ENDPOINTS.COLUMNS);
  }

  setColumnCapacity(id, capacity) {
    return axios.patch("column/" + id + "/capacity/" + capacity);
  }

  addCard(card) {
    return axios.post(ENDPOINTS.CARDS, card);
  }

  statusChange(id, status) {
    return axios.patch("card/" + id + "/status/" + status);
  }

  setOrder(ordered) {
    return axios.patch(ENDPOINTS.CARDS_ORDER, ordered);
  }

  removeCard(id) {
    return axios.delete(ENDPOINTS.CARDS + id);
  }
}

const myService = new MyService();
export default myService;
