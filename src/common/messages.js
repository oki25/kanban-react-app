const messages = {
  capacityMax:
    "Column has reached maximum capacity, please set capacity first!",
  error: "Oooops! Something went wrong! Please try again later!",
  capacityValue:
    "You entered wrong value, insert number greater or equals to 0!",
  titleDesc: "Title and description are required!",
  create: "Card created successfuly!",
  delete: "Card has been removed!"
};
export default messages;
