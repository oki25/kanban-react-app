import React, { PureComponent } from "react";
import Card from "./Card";
import MyService from "../services/MyService";
import AddCard from "./AddCard";
import { DropTarget } from "react-dnd";
import m from "../common/messages";

const update = require("immutability-helper");

const columnTarget = {
  drop: function(props, monitor) {
    const card = monitor.getItem().card;
    const column = props.column;
    const ordered = props.column.cards;
    props.handleDrop(card, column, ordered);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
    item: monitor.getItem()
  };
}

class Column extends PureComponent {
  state = {
    isHidden: true,
    capacitySetterHidden: true
  };

  update = () => {
    this.props.update();
  };

  openCloseForm = () => {
    const isHidden = !this.state.isHidden;
    this.setState({ isHidden });
  };

  setCapacity = () => {
    const value = this.state.newCapacity;
    if (value === "" || isNaN(value) || value < 0) {
      alert(m.capacityValue);
      return;
    }

    const columnId = this.props.column.id;
    MyService.setColumnCapacity(columnId, value)
      .then(res => {
        this.props.update();
        this.openCloseSetCapacity();
      })
      .catch(error => {
        alert(m.error);
      });
  };

  openCloseSetCapacity = () => {
    const capacitySetterHidden = !this.state.capacitySetterHidden;
    this.setState({ capacitySetterHidden });
  };

  onChangeSetInput = e => {
    this.setState({ newCapacity: e.target.value });
  };

  moveCard = (dragIndex, hoverIndex) => {
    const cards = this.props.column.cards;
    const temp = cards[dragIndex];
    cards[dragIndex] = cards[hoverIndex];
    cards[hoverIndex] = temp;

    const tempPos = cards[dragIndex].position;
    cards[dragIndex].position = cards[hoverIndex].position;
    cards[hoverIndex].position = tempPos;

    update(this.props.column, {
      cards: {
        $set: cards
      }
    });
    this.forceUpdate();
  };

  render() {
    const { connectDropTarget, hovered, column } = this.props;
    const backgroundColor = hovered ? "lightgreen" : "white";
    const cards =
      column.capacity !== null
        ? column.cards.slice(0, column.capacity)
        : column.cards;
    const capacity = column.capacity;
    const length = cards.length;
    return connectDropTarget(
      <div className="column" style={{ background: backgroundColor }}>
        <h1>
          {column.name}
          {capacity === null ? <span /> : <span>({column.capacity})</span>}
        </h1>
        <button
          className="glyphicon glyphicon-pencil"
          onClick={this.openCloseSetCapacity}
        />
        {!this.state.capacitySetterHidden && (
          <div>
            <label>Set column capacity</label> <br />
            <input type="text" onChange={this.onChangeSetInput} />{" "}
            <button onClick={this.setCapacity}>Set</button>
            <hr />
          </div>
        )}
        {cards.map((card, i) => (
          <Card
            key={card.id}
            card={card}
            moveCard={this.moveCard}
            setOrder={this.setOrder}
            index={i}
            update={this.update}
          />
        ))}
        {!this.state.isHidden && (
          <AddCard
            column={this.props.column}
            update={this.update}
            closeForm={this.openCloseForm}
          />
        )}
        <hr />

        {this.state.isHidden ? (
          <button
            className="btn btn-info"
            onClick={this.openCloseForm}
            disabled={capacity === length}
          >
            Add new card
          </button>
        ) : (
          <button className="btn btn-danger" onClick={this.openCloseForm}>
            Cancel and Close
          </button>
        )}
      </div>
    );
  }
}

export default DropTarget("card", columnTarget, collect)(Column);
