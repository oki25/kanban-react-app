import React, { PureComponent } from "react";
import Column from "./Column";
import MyService from "../services/MyService";
import HTML5Backend from "react-dnd-html5-backend";
import { DragDropContext } from "react-dnd";
import m from "../common/messages";

class Kanban extends PureComponent {
  state = {
    columns: []
  };

  getBoardData() {
    MyService.getBoardData()
      .then(res => {
        this.setState({ columns: res.data.columns });
      })
      .catch(error => {
        alert(m.error);
      });
  }

  componentDidMount() {
    this.getBoardData();
  }

  updateStatus = () => {
    this.getBoardData();
  };

  setCardStatus = (card, column) => {
    MyService.statusChange(card.id, column.id)
      .then(res => {
        if (res.data === 403) {
          alert(m.capacityMax);
          return;
        }
        this.getBoardData();
      })
      .catch(error => {
        alert(m.error);
      });
  };

  setOrder = ordered => {
    MyService.setOrder(ordered)
      .then(res => {})
      .catch(error => {
        alert(m.error);
      });
  };

  handleDrop = (card, column, ordered) => {
    card.status === column.id
      ? this.setOrder(ordered)
      : this.setCardStatus(card, column);
  };

  render() {
    return (
      <div className="kanban">
        {this.state.columns.map(column => (
          <Column
            key={column.id}
            column={column}
            update={this.updateStatus}
            handleDrop={this.handleDrop}
          />
        ))}
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(Kanban);
