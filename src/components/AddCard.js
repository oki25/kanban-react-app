import React, { PureComponent } from "react";
import MyService from "../services/MyService";
import m from "../common/messages";

class AddCard extends PureComponent {
  state = {
    title: "",
    description: "",
    status: this.props.column.id
  };

  updateColumn = () => {
    this.props.update();
    this.props.closeForm();
  };
  addTask = () => {
    if (this.state.title === "" || this.state.description === "") {
      alert(m.titleDesc);
      return;
    }
    MyService.addCard(this.state)
      .then(res => {
        if (res.data === 403) {
          alert(m.capacityMax);
          return;
        }
        alert(m.create);
        this.updateColumn();
      })
      .catch(error => {
        alert(m.error);
      });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.addTask();
    this.setState({ title: "", description: "" });
  };

  onTitleChange = e => {
    this.setState({
      title: e.target.value
    });
  };

  onDescriptionChange = e => {
    this.setState({
      description: e.target.value
    });
  };

  render() {
    return (
      <div>
        <form>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              placeholder="Enter title"
              onChange={this.onTitleChange}
              value={this.state.title}
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              className="form-control"
              id="description"
              placeholder="Enter description"
              onChange={this.onDescriptionChange}
              value={this.state.description}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.handleSubmit}
          >
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default AddCard;
