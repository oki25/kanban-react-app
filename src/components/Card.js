import React, { PureComponent } from "react";
import { DragSource, DropTarget } from "react-dnd";
import { findDOMNode } from "react-dom";
import MyService from "../services/MyService";
import m from "../common/messages";
import flow from "lodash/flow";

const cardSource = {
  beginDrag(props) {
    return {
      card: props.card,
      index: props.index
    };
  },
  endDrag(props, monitor, component) {
    if (!monitor.didDrop()) {
      return;
    }
  }
};

const cardTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%
    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Only perform the move when the
    if (props.position === monitor.getItem().position) {
      props.moveCard(dragIndex, hoverIndex);
    }

    monitor.getItem().index = hoverIndex;
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  };
}
class Card extends PureComponent {
  state = {
    infoHidden: true
  };

  toggleInformation = () => {
    const infoHidden = !this.state.infoHidden;
    this.setState({ infoHidden });
  };

  removeCard = () => {
    const id = this.props.card.id;
    MyService.removeCard(id)
      .then(res => {
        this.props.update();
        alert(m.delete);
      })
      .catch(error => {
        alert(m.error);
      });
  };

  render() {
    const {
      card,
      isDragging,
      connectDragSource,
      connectDropTarget
    } = this.props;
    const opacity = isDragging ? 0 : 1;
    return (
      connectDragSource &&
      connectDropTarget &&
      connectDragSource(
        connectDropTarget(
          <div
            className="card"
            onDoubleClick={this.toggleInformation}
            style={{ opacity }}
          >
            <h4>{card.title}</h4>
            {this.state.infoHidden ? null : (
              <div>
                <p> {card.description} </p>
                <small>Created at: {card.created_at}</small> <hr />
                <button
                  className="btn btn-danger"
                  onClick={card => this.removeCard(card)}
                >
                  Remove
                </button>
              </div>
            )}
          </div>
        )
      )
    );
  }
}

export default flow(
  DragSource("card", cardSource, collect),
  DropTarget("card", cardTarget, connect => ({
    connectDropTarget: connect.dropTarget()
  }))
)(Card);
