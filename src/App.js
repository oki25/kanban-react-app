import React, { Component } from "react";
import Kanban from "./components/Kanban";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="myClass">
          <h1>Kanban board</h1>
        </div>
        <Kanban />
      </div>
    );
  }
}

export default App;
